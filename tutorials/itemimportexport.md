# Items importieren und exportieren

---

## Items importieren

---

Sie haben die Möglichkeit, Items nach EAs.LiT zu importieren. Klicken Sie dafür unter **Import und Export von Items** auf **Items importieren**. Wählen Sie nun die Datei aus, in welchem sich das zu importierende Item befindet. Zur Auswahl stehen:
* CSV
* Microsoft Excel
* ZIP-Archiv
* Ilias

Sofern Sie ein Häkchen bei **Expertenmodus** setzen, haben Sie zusätzliche Dateiformate zur Auswahl. Diese zusätzlichen Formate sind für spezielle Anwendungszwecke gedacht und decken teilweise nicht den vollen Umfang der anderen Formate ab. Die zusätzlichen Formate sind:
* Gezippte CSV
* Gezippte Microsoft Excel
* JSON
* Gezippte JSON
* CSV JSON

Ist das Format Ihrer ausgewählten Datei eindeutig erkennbar, wählt EAs.LiT automatisch das richtige Dateiformat für den Import aus. Klicken Sie nun auf **Importieren**. Das Item ist nun Teil Ihres Projektes.


## Items exportieren

---

Klicken Sie unter **Import und Export von Items** auf **Items exportieren**, werden Sie zur [Item Liste](tutorials/itemlist.md) weitergeleitet. Hier können Sie die Items, die Sie exportieren möchten, auswählen. Klicken Sie zum Auswählen eines Items einmal darauf. Ausgewählte Items werden dunkelgrau unterlegt und erhalten in der ersten Spalte einen Haken. Indem Sie ein zweites Mal auf ein Item klicken, wird die Auswahl dieses Items aufgehoben. Haben Sie die Items, die Sie exportieren möchten, ausgewählt, klicken Sie oben auf **Exportieren**. Wählen Sie nun das Format aus, in welchem die ausgewählten Items heruntergeladen werden sollen. Mit einem Klick auf **Ok** startet der Download.
