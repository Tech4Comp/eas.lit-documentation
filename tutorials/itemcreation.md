<h2> Inhalt </h2>

---

[toc]

<br/>

# Itemerstellung und -bearbeitung

---

Um ein neues Item zu erstellen oder ein bestehendes zu bearbeiten, müssen Sie die unterschiedlichen Eingabefelder ausfüllen. Dabei ist die Auswahl eines *Learning Outcomes*, der *Levels nach Bloom* sowie von *Themen* optional. Die restlichen Eingabefelder müssen ausgefüllt werden. Die Eingabefelder werden folgend genau erläutert. Vor dem Speichern des Items muss dieses validiert werden, dafür wird die Schaltfläche **Validieren** betätigt. Ist das Formular korrekt ausgefüllt, kann das Item mit einem Klick auf die **Speichern**-Schaltfläche gespeichert werden. Die Schaltfläche **Item-Vorschau** bietet eine Vorschau der Aufgabe, so wie sie den Lernenden präsentiert wird:

![alt text](../tutorial_img/vorschau.jpg "Item-Vorschau")

<br/>

# Eingabefelder

---

#### Item-Typ
Beim Item Typ wird ausgewählt, um was für einen Aufgabentyp es sich beim zu erstellenden Item handeln soll. Ausgewählt wird zwischen *Single Choice*, *Remote*, *Multiple Choice*, *Free Text*, *Assignment* und *Arrangement*. Die verschiedenen Aufgabentypen werden weiter unten genauer erläutert.

#### Status
Der Status zeigt an, ob ein Item _veröffenticht_, _bearbeitet_, _zu bearbeiten_, _zurückgestellt_ oder ein _Entwurf_ ist. Unter *Items begutachten* finden Sie eine Übersicht mit den unterschiedlichen Status und ihren zugehörigen Items. Dort haben Sie die Möglichkeit, den Status eines Items zu ändern.

#### Learning Outcome

Learning Outcomes beschreiben die Kompetenzen, welche der:die Lernende im Rahmen einer Lernaktivität erwerben soll. Ein bestehendes Learning Outcome kann entweder aus der Dropdown-Liste ausgewählt werden, oder Sie legen ein neues Learning Outcome an. Die Zuordnung eines Items zu einem Learning Outcome ist nicht zwingend erforderlich, wird aber empfohlen. Wenn Sie ein Learning Outcome auswählen, werden die Themen und Levels nach Anderson & Krathwohl dieses Learning Outcomes automatisch für das Item übernommen.

#### Titel, Vignette und Aufgabe

Mit den Elementen Titel, Vignette und Aufgabe wird die Aufgabenstellung konkretisiert. Der Titel beschreibt das Item in wenigen Worten. Er dient dazu, das Item schnell wiederzuerkennen. Die Vignette beschreibt den Kontext, den der:die Lernende benötigt, um die Aufgabe lösen zu können. Das Element Aufgabe beinhaltet dann die tatsächliche Aufgabenstellung, die den:die Lernende:n auffordert, etwas zu tun. Hier ein Beispiel:

![alt text](../tutorial_img/aufgabenstellung.jpg "Titel, Vignette und Aufgabe")

#### Empirische Schwierigkeit (0-100)

Die empirische Schwierigkeit eines Items wird aus den Prüfungsergebnissen zurückgerechnet. Die Schwierigkeit der Aufgabe wird als Wert zwischen 0 und 100 beschrieben. An dieser Stelle müssen Sie nichts angeben, da dieser Wert automatisch ermittelt wird.

#### Erwartete Lösungswahrscheinlich (0.1-1)

Als Wert zwischen 0,1 und 1 beschreibt die erwartete Lösungswahrscheinlichkeit, wie viele Lernende die Aufgabe wahrscheinlich lösen können. Der Wert 1 bedeutet, dass alle Lernenden diese Aufgabe lösen können, während bei einer erwarteten Lösungswahrscheinlichkeit von 0,1 nur wenige Lernende die Aufgabe lösen können. Dieser Wert wird von Ihnen auf einem Schieberegler festgelegt:

![alt text](../tutorial_img/loesungswhr.jpg "Erwartete Lösungswahrscheinlichkeit")

#### Levels nach Anderson & Krathwohl

Mithilfe von Anderson & Krathwohls Taxonomiestufen können Items innerhalb einer Matrix Kategorien zugeordnet werden. Die Matrix ist folgendermaßen aufgebaut: Auf der X-Achse befinden sich vier Wissensdimensionen, während die Y-Achse die kognitive Dimension zeigt.

![alt text](../tutorial_img/bloom_matrix.jpg "Levels nach Bloom")

Indem Sie an den passenden Stellen Kreuze setzen, können Sie das Item innerhalb von Anderson & Krathwohls Taxonomiemodell lokalisieren. Ein Kreuz deutet immer auf eine kognitive Dimension der Aufgabe, die innerhalb einer Wissensdimension verortet ist.   

Die **sechs kognitiven Dimensionen** lauten:
* **Erinnern:**
   Die Lernenden können erlerntes Faktenwissen wiedergeben (Begriffe, Definitionen, Daten, Abläufe usw.).
* **Verstehen:**
   Die Lernenden können beispielsweise einen Sachverhalt oder einen Begriff mit eigenen Worten erklären, verstehen Zusammenhänge und können diese graphisch darstellen, oder führen eigene Beispiele an.    
* **Anwenden:**
   Die Lernenden wenden etwas Gelerntes (Methode, Regel, Wissen usw.) in einem neuen Kontext oder einer neuen Situation an, die zuvor nicht vorgekommen ist.   
* **Analysieren:**
   Die Lernenden können Zusammenhänge erkennen und Folgerungen ableiten. Modelle oder Verfahren werden in ihre Bestandteile zerlegt, wobei Strukturen aufgedeckt werden.
* **Evaluieren:**
   Die Lernenden können ein Modell, ein Verfahren oder eine Lösung beurteilen. Dabei werden beispielsweise Alternativen hinsichtlich verschiedener Faktoren gegeneinander abgewägt und Entschlüsse gefasst und begründet.    
* **Erschaffen:**
   Die Lernenden können konstruktiv Elemente und Informationen aus Teilen zusammenfügen oder neu ordnen, sodass neue Schemata, Hypothesen usw. entstehen. Sie erbringen eine schöpferische Leistung, da ihnen die neu entwickelten Elemente in der Form nicht gelehrt worden sind.

Die **Wissensdimensionen** lauten:
* **Konzeptuell:**
   Wechselbeziehungen zwischen Grundelementen innerhalb einer Struktur, die es ihnen ermöglichen, zusammen zu funktionieren.     
* **Fakt:**
   Grundlegende Elemente, die die Lernenden kennen müssen, um mit einer Disziplin vertraut zu sein und Probleme in ihr zu lösen.
* **Prozess:**
   Kenntnisse über themenspezifische Fähigkeiten, Algorithmen, Methoden und Techniken sowie Kenntnis der Kriterien zur Bestimmung geeigneter Verfahren.
* **Metacognitiv:**
   Wissen über Kognition im Allgemeinen sowie Bewusstsein und Wissen über die eigene Wahrnehmung

Wenn Sie Ihrem Item ein Learning Outcome hinzugefügt haben, werden die Kreuze aus der Matrix des Learning Outcomes direkt für dieses Item übernommen.

#### Themen

Ein Thema ist vergleichbar mit einem Schlagwort. Ein Item kann mit mehreren Themen versehen werden, die beispielsweise beim Filtern von Items nützlich sein können.

Es besteht die Möglichkeit, ein Thema aus einer Dropdown-Liste bereits vorhandener Themen auszuwählen:

![alt text](../tutorial_img/thema_auswahl.jpg "Auswahl eines vorhandenen Themas")

Es können aber auch neue Themen erstellt werden:

![alt text](../tutorial_img/thema_neu.jpg "Neues Thema erstellen")

Wenn Sie Ihrem Item ein Learning Outcome hinzugefügt haben, werden die Themen des Learning Outcomes direkt für dieses Item übernommen.

<br/>

# Aufgabentypen

---

#### Single Choice

Der Aufgabentyp Single Choice erlaubt das Ankreuzen von genau einer Antwort. Dabei ist eine Antwortmöglichkeit korrekt, für sie gibt es die meisten Punkte. Die anderen Antwortmöglichkeiten sind möglicherweise teilweise korrekt, weshalb für sie auch Punkte vergeben werden können. Hier ein Beispiel:

![alt text](../tutorial_img/sc_points.jpg "Punkteverteilung Single Choice")

Auf die korrekte Antwort, *USA vs. Niederlande*, gibt es fünf Punkte. Zwei weitere Antwortmöglichkeiten beinhalten jeweils eine der Mannschaften, die im Finale angetreten sind. Auf diese Antwortmöglichkeiten gibt es jeweils zwei Punkte für eine teilweise korrekte Antwort. Eine vierte Antwortmöglichkeit ist falsch, für sie werden null Punkte vergeben.  

#### Multiple Choice

Der Aufgabentyp Multiple Choice erlaubt das Ankreuzen mehrerer Antworten. Bei der Punkteverteilung wird zwischen Positiv- und Negativantworten unterschieden. Eine Positivantwort entspricht "angekreuzt", eine Negativantwort entspricht "nicht angekreuzt". So können auch Punkte für das Nicht-Ankreuzen von Antwortmöglichkeiten vergeben werden. Hier ein Beispiel:

![alt text](../tutorial_img/mc_points.jpg "Punkteverteilung Multiple Choice")

Werden die beiden korrekten Antworten *USA* und *Niederlande* ausgewählt, können insgesamt 14 Punkte erreicht werden (jeweils fünf Punkte für die Auswahl der korrekten Antworten und jeweils zwei Punkte für das Nicht-Ankreuzen der inkorreten Antwortmöglichkeiten). Werden aber beispielsweise die Antwortmöglichkeiten *England* und *USA* ausgewählt, so erhält die zu testende Person immer noch fünf Punkte für eine korrekte Antwort sowie zwei Punkte für das Nicht-Ankreuzen einer falschen Antwort, in diesem Fall *Schweden*.

_Minimal- und Maximalanzahl Antworten:_
Geben Sie hier die minimale bzw. maximale Anzahl der Antworten, die die Lernenden ankreuzen sollen, an. Das dient dazu, den Studierenden Hinweise auf die Anzahl der anzukreuzenden Antworten zu geben. Im obigen Beispiel wäre die maximale Anzahl *zwei*. Die Lernenden kriegen dann einen Hinweis: "Wählen Sie maximal 2 der 4 Antwortoptionen aus." Als minimale Anzahl wählen Sie eins, sodass die Lernenden den Hinweis kriegen: "Wählen Sie mindestens 1 der 4 Antwortoptionen aus."

#### Free Text

Bei Freitextaufgaben kann ausgewählt werden, wie viele mögliche Punkte die Aufgabe insgesamt bringen soll. Der Bereich der möglichen Punktzahl liegt zwischen eins und zehn. Die Punktzahl wird auf einem Schieberegler festgelegt:

![alt text](../tutorial_img/ft_points.jpg "Punktevergabe Freitextaufgaben")

Das Freitextfeld für die Bearbeitung der Aufgabe wird automatisch generiert.

#### Arrangement

Ein Arrangement-Item ist als Anordnungsaufgabe zu verstehen. Die Lernenden sollen Elemente nach bestimmten Kriterien sortieren, indem sie den Elementen Nummern zuordnen. Diese Kriterien sollten in der Aufgabenstellung genau beschrieben werden, damit die Lernenden wissen, wie die Antworten zu nummerieren sind. Sie sollten in der Aufgabe auch klar formulieren, ob ab- oder aufsteigend sortiert werden soll. Im folgenden Beispiel sollen die Lernenden die Ereignisse in eine chronologische Reihenfolge bringen, beginnend bei eins mit dem frühesten Ereignis.

![alt text](../tutorial_img/arrangement.jpg "Punktevergabe und Platzierung Arrangement")

Die Zahlenspalte auf der linken Seite gibt die Punkzahl bei korrekter Platzierung des Elements an. Hier werden für jede korrekte Platzierung fünf Punkte vergeben. In der Zahlenspalte auf der linken Seite tragen Sie die korrekte Platzierung ein. In diesem Beispiel verteilen Sie also die Zahlen eins bis vier auf die passenden Antworten, beginnend mit dem *Ausbruch der Spanischen Grippe (1)*.   

#### Remote

Remote Items sind externe Items, für die nur Metadaten in EAs.LiT verwaltet werden. Die Zuordnung der Items zu Learning Outcomes und zum Domänenmodell wird von EAs.LiT übernommen. Im Gegensatz zu den anderen Aufgabentypen benötigen sie nur Titel, die Remote-URL des Items, die erwartete Lösbarkeit der Aufgabe, sowie eine Zuordnung der Levels nach Anderson & Krathwohl.  


#### Assignment

*Noch nicht implementiert.*
