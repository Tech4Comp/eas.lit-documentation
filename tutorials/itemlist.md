<h2> Inhalt </h2>

---

[toc]

<br/>

# Item Liste

---

Wenn Sie sich alle Items eines Projektes übersichtlich anzeigen lassen möchten, können Sie dies in der **Item Liste** tun. Hier werden alle Items mit Item Nummer, Item Typ, Titel, Vignette, Aufgabe und Erstellzeitpunkt dargestellt. Sie haben die Möglichkeit, Items zu suchen und zu filtern. Einzelne Items können hier bearbeitet oder gelöscht werden. Klicken Sie ein Item an, wird es in die Liste der Items aufgenommen, die Sie herunterladen möchten. Weiterhin können Items dem **Item-Korb** hinzugefügt werden, aus dem später Prüfung generiert werden.

# Items suchen und filtern

---

#### Items suchen

Um ein Item zu suchen, klicken Sie oben in das Textfeld **Suchen in Titel & Aufgabe** und tippen Sie ihren Suchbegriff. Bei allen Items wird nun der *Titel*, die *Vignette* und die *Aufgabe* nach dem Begriff durchsucht. Die gefundenen Items werden in der Liste angezeigt. **_Wichtig:_** Sie können *nicht* mehrere Suchbegriffe eingeben. Die Suchfunktion unterstützt keine logischen Verknüpfungen wie *AND*, *OR* oder *NOT*. Die Groß- bzw. Kleinschreibung Ihres Suchbegriffes ist egal.

#### Items filtern

Um nur bestimmte Items anzeigen zu lassen, klicken Sie auf **Filter anzeigen**. Nun können Sie alle Items nach bestimmten Kategorien filtern:
* **Markierung:** Es werden nur markierte Items angezeigt.
* **Item Typ:** Nur die ausgewählten Item Typen werden in der Liste angezeigt.
* **Erstellt:** Nur Items, die vor oder nach einem gewählten Datum erstellt wurden, werden angezeigt.
* **Letzte Modifikation:** Items, die vor oder nach einem gewählten Datum bearbeitet wurden, werden angezeigt.
* **Themen:** Nur Items, denen eines der ausgewählten Themen zugeordnet wurden, werden angezeigt.

Natürlich können Sie die Item Liste auch nach mehreren Kategorien filtern, sodass beispielsweise nur markierte Items angezeigt werden, die nach dem 31.08.2020 erstellt wurden.

#### Items sortieren

In der Liste können die Items auch sortiert werden. Dafür gibt es zwei Möglichkeiten:
* eine auf- bzw. absteigende Sortierung nach der **Item Nummer**.
* eine auf- bzw. absteigende Sortierung nach dem **Item Typ** (alphabetisch).

<br/>

# Aktionen

---

![alt text](../tutorial_img/aktionen.jpg "Aktionsmöglichkeiten für einzelne Items")

#### Bearbeiten

Klicken Sie auf das blaue **Bearbeiten**-Icon eines Item, gelangen Sie in den Bearbeitungsmodus dieses Items. Eine genaue Erklärung, wie Sie Items bearbeiten, finden Sie [hier](tutorials/itemcreation.md).

#### Zum Item-Korb hinzufügen

Mit dem mittleren Button fügen Sie ein Item dem Item-Korb hinzu. Sie können dem Item-Korb beliebig viele Items hinzufügen. Aus diesen Items kann später eine Prüfung generiert werden. Eine genaue Erklärung dazu, was der Item-Korb ist und wie Sie damit eine Prüfung generieren, finden Sie [hier](tutorials/itembasket.md).

#### Löschen

Mit der unteren Schaltfläche können Sie ein Item unwiderruflich löschen. **_Achtung:_** Es gibt keinen Papierkorb! Gelöschte Items können nicht wiederhergestellt werden. Klicken Sie auf **Löschen**, öffnet sich ein Fenster, welches Ihnen die Möglichkeit bietet, das Item vor dem Löschen in der Export-Ansicht herunterzuladen. Hier können Sie den Prozess immer noch abbrechen oder das Item unwiderruflich löschen.  

#### Items exportieren

Sie können Items direkt exportieren. Lesen Sie [hier](tutorials/itemimportexport.md) nach, wie Sie Items aus der Item Liste exportieren.
