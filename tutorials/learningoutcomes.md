<h2> Inhalt </h2>

---

[toc]

<br/>

# Learning Outcomes

---

**Learning Outcomes** beschreiben die Kompetenzen, welche der:die Lernende im Rahmen einer Lernaktivität erwerben soll. Sie können bei er Erstellung oder Bearbeitung eines Items diesem hinzugefügt werden. Unter **Learning Outcomes** wird Ihnen eine Übersicht mit allen bisher erstellten Learning Outcomes sowie die Möglichkeit, diese zu _bearbeiten_, zu _löschen_ oder neue Learning Outcomes zu _erstellen_, geboten.
Die Übersicht zeigt alle bisher erstellen Learning Outcomes als Kachel an:

![alt text](../tutorial_img/learningoutcome.jpg "Anzeige eines Learning Outcomes")

Neben dem **Titel** und der **Beschreibung** des Learning Outcomes werden zusätzlich die individuelle **Nummer** des Learning Outcomes, das **Datum** der Erstellung bzw. der letzten Bearbeitung sowie die **Anzahl** der zugeordneten Items dieses Learning Outcomes angezeigt. Mit einem Klick auf die **rote Schaltfläche** löschen Sie das Learning Outcome. Mit einem Klick auf die **blaue Schaltfläche** gelangen Sie in den Bearbeitungsmodus des Learning Outcomes.

<br/>

# Anlegen eines Learning Outcomes

---

Mit einem Klick auf die Schaltfläche **Anlegen...** erstellen Sie ein neues Learning Outcome. Um ein neues Learning Outcome anzulegen, füllen Sie das Formular vollständig aus und klicken Sie anschließend auf *Anlegen*.

#### Titel

Geben Sie dem Learning Outcome einen **Titel**. Er sollte gut zu der Kompetenz, die bei diesem Learning Outcome erreicht werden soll, passen. Damit finden Sie das Learning Outcome später schnell wieder.  

#### Beschreibung

Es folgt die **Beschreibung** des Learning Outcomes. Hier soll verständlich in einem oder mehreren Sätzen beschrieben werden, was die Lernenden bei einer Aufgabe, der dieses Learning Outcome zugeordnet ist, können müssen.

#### Taxonomiestufen nach Anderson & Krathwohl

Mithilfe der **Taxonomiestufen nach Anderson & Krathwohl** wird das Learning Outcome zusätzlich innerhalb eines Matrix kategorisiert. Zum einen werden Wissensdimensionen, zum anderen kognitive Dimensionen zugeordnet. Eine genaue Beschreibung der einzelnen Dimensionen finden Sie [hier](tutorials/itemcreation.md) unter *Levels nach Anderson & Krathwohl*.

#### Themen

Ein **Thema** ist vergleichbar mit einem Schlagwort. Ein Learning Outcome kann mit mehreren Themen versehen werden. Diese Themen werden dann automatisch auch einem Item, dem Sie das Learning Outcome zuordnen, zugeteilt. Das hilft beispielsweise beim Filtern von Items und kann bei der Prüfungserzeugung ausgenutzt werden.
