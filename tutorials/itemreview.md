<h2> Inhalt </h2>

---

[toc]

<br/>

# Itembegutachtung

---

Die Ansicht **Items begutachten** bietet eine Übersicht über die bisher erstellen Items, kategorisiert nach ihrem Status:
* **Neue Items**
* **Zu überarbeitende Items**
* **Überarbeitete Items**
* **Zurückgestellte Items**  

Mit einem Klick auf eine der Kacheln werden Sie in eine Übersicht mit den zum Status zugehörigen Items geleitet. In der Übersicht sehen Sie eine Liste der Items des gewählten Status. Die Liste kann zusätzlich gefiltert werden. Die Kategorien zum Filtern der Items sind:
* **Markierung:** Es werden nur markierte Items angezeigt.
* **Item Typ:** Nur die ausgewählten Item Typen werden in der Liste angezeigt.
* **Erstellt:** Nur Items, die vor oder nach einem gewählten Datum erstellt wurden, werden angezeigt.
* **Letzte Modifikation:** Items, die vor oder nach einem gewählten Datum bearbeitet wurden, werden angezeigt.
* **Themen:** Nur Items, denen eines der ausgewählten Themen zugeordnet wurden, werden angezeigt.

Mit einem Klick auf ein Item aus der Liste wird eine Vorschau des Items, so wie es den Lernenden als Aufgabe präsentiert wird, geöffnet. Die Vorschau bietet gleichzeitig unterschiedliche _Optionen zur Begutachtung des Items_.  

<br/>

# Optionen zur Begutachtung der Items

---

#### Veröffentlichen:

Wird ein Item **veröffentlicht**, kann es für Prüfungen verwendet werden. Mögliche Markierungen und Notizen werden dann entfernt. Veröffentlichte Items werden nicht mehr in der **Items begutachten**-Ansicht angezeigt. In der Item Liste, zu der Sie gelangen, wenn Sie in der Projektübersicht auf **Items** klicken, werden veröffentlichte Items angezeigt und können bearbeitet werden. Wird ein veröffentlichtes Item bearbeitet, wechselt der Status auf **Überarbeitet**.

#### Zurückstellen:

Ein Item wird **zurückgestellt**, wenn es nicht zur Prüfungserzeugung zugelassen ist. Wählen Sie diese Option, öffnet sich ein Fenster, in dem Sie in wenigen Worten einen Grund angeben, weshalb das Item zurückgestellt wurde.
Das zurückgestellte Item finden Sie nun in der Begutachten-Übersicht unter **Zurückgestellte Items**. Bearbeiten Sie ein zurückgestelltes Item, wird der Status nach dem Speichern _nicht_ verändert. Der Status kann ausschließlich in der Begutachten-Übersicht geändert werden.

#### Markieren:

Items können **markiert** werden. Eine Markierung weist darauf hin, dass ein Item an einer oder mehreren Stellen nochmals überarbeitet werden muss. Wenn Sie ein Item markieren, öffnet sich ein Fenster, in dem Sie kurz beschreiben, weshalb Sie das Item markiert haben und an welchen Stellen es überarbeitet werden muss (ähnlich wie **Items Zurückstellen**). Der Status des Items wird dann automatisch auf **Zu überarbeitende Items** gesetzt. Dort finden Sie das Item nach dem Markieren und können dort die Markierung wieder entfernen. Entfernen Sie die Markierung, wird der Status wieder auf **Überarbeitete Items** gesetzt. Sie haben auch die Möglichkeit, den Status des Items zu verändern, ohne die Markierung aufzuheben. Wenn Sie das markierte Item bearbeiten, bleibt die Markierung und die zugehörige Notiz bestehen.

#### Bearbeiten:

Klicken Sie auf **Bearbeiten**, werden Sie automatisch in die Bearbeiten-Übersicht geleitet (siehe [Itemerstellung und -bearbeitung](tutorials/itemcreation.md)). Bearbeiten Sie das Item, wird der Status auf **Überarbeitet** gesetzt.

#### Löschen:

Hier können Sie ein Item unwiderruflich löschen. _Achtung:_ Es gibt keinen Papierkorb! Gelöschte Items können nicht wiederhergestellt werden. Klicken Sie auf **Löschen**, öffnet sich ein Fenster, was Ihnen die Möglichkeit bietet, das Item vor dem Löschen in der Export-Ansicht herunterzuladen. Hier können Sie den Prozess immer noch abbrechen oder das Item unwiderruflich löschen.  
