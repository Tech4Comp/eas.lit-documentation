# Item-Korb und Prüfungsgenerierung

---

Wenn Sie die gewünschten Items aus der [Item Liste](tutorials/itemlist.md) dem Item-Korb hinzugefügt haben, finden Sie oben im Reiter der Anwendung ein Korb-Icon mit der Anzahl der Items, die sich im Item-Korb befinden.

![alt text](../tutorial_img/item_korb.jpg "Item-Korb")

Der Item-Korb kann außerdem über die Startseite des Projektes erreicht werden.

Mit einem Klick auf das Icon gelangen Sie in den Item-Korb. Dort sind alle Items gelistet, die Sie dem Item-Korb hinzugefügt haben. Hier haben Sie die Möglichkeit, Items wieder aus dem Item-Korb zu entfernen, indem Sie auf das entsprechende Item klicken:

![alt text](../tutorial_img/item_entfernen.jpg "Item aus dem Item-Korb entfernen")

Zusätzlich sehen Sie in einem Balkendiagramm die Verteilung der Items, kategorisiert nach Item Typ. Klicken Sie auf **Prüfung generieren**, werden Sie auf eine Seite weitergeleitet, auf der Sie spezifizieren können, wie viele Items eine Prüfung enthalten soll und wie viele Prüfungen aus dem Item-Korb erzeugt werden sollen. Weiterhin können Sie eingrenzen...
* ...wie viele Items minimal und maximal aus welchem Item-Typ in der Prüfung vertreten sein sollen,
* ...wie viele Items minimal und maximal aus welcher Wissendimension vertreten sein sollen,
* ...wie viele Items minimal und maximal aus welcher Anforderungsstufe vertreten sein sollen und
* ...wie viele Items minimal und maximal mit welchem Learning Outcome vertreten sein sollen.

Hier ein Beispiel:

![alt text](../tutorial_img/pruefungsgenerierung.jpg "Prüfung generieren")

In diesem Beispiel sollen insgesamt fünf Prüfungen mit jeweils vier Items generiert werden. Dabei sollen mindestens ein und maximal vier Single Choice Items, maximal vier Multiple Choice Items sowie minimal drei und maximal sechs Free Text Items Teil jeder der vier Prüfungen sein. Eine Prüfung bestünde demnach zum Beispiel aus drei Free Text Items und einem Single Choice Item. Weiterhin sollen die Prüfungen minimal keine und maximal neun Items, die mit dem Learning Outcome _Wetter_ versehen sind, enthalten.

Mit einem Klick auf **Prüfung generieren** öffnet sich ein Fenster. Hier wählen Sie das Format, in welchem die Prüfung heruntergeladen werden soll. Sie haben die Auswahl zwischen:
* CSV
* Microsoft Excel
* ZIP-Archiv
* Ilias

Machen Sie ein Häkchen bei **Expertenmodus**, haben Sie zusätzliche Downloadformate zur Auswahl. Diese zusätzlichen Formate sind für spezielle Anwendungszwecke gedacht und decken teilweise nicht den vollen Umfang der anderen Formate ab. Die zusätzlichen Formate sind:
* Gezippte CSV
* Gezippte Microsoft Excel
* JSON
* Gezippte JSON
* CSV JSON
